const MY_IP = process.env.IP || "localhost";
const PORT = process.env.PORT || 9800;
const LOGIN_NAME = process.env.LOGIN_NAME || "panShi";
const LOGIN_PASSWORD = process.env.LOGIN_PASSWORD || "panShi213";
const RTSP_AUTH = process.env.RTSP_AUTH || "tciapp:187icT%23%24%25";
const RTSP_IP = process.env.RTSP_IP || "61.216.125.61";

console.log("==================================");
console.log("MY_IP", MY_IP);
console.log("PORT", PORT);
console.log("LOGIN_NAME", LOGIN_NAME);
console.log("LOGIN_PASSWORD", LOGIN_PASSWORD);
console.log("RTSP_AUTH", RTSP_AUTH);
console.log("RTSP_IP", RTSP_IP);
console.log("==================================");

const express = require("express");
const cors = require("cors");
const app = express().use("*", cors());
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const compression = require("compression");
app.use(express.json());
app.use(compression({ filter: shouldCompress })); //壓縮加速
const { proxy } = require("rtsp-relay")(app);

function getTimeStr() {
  return new Date().toTimeString();
}

function shouldCompress(req, res) {
  if (req.headers["x-no-compression"]) {
    // don't compress responses with this request header
    return false;
  }
  return compression.filter(req, res);
}
const path = require("path");
const docs = path.join(__dirname, "../", "docs");
app.use(express.static(docs));
app.listen(PORT, () => {
  console.log(`Server Listening on port ${PORT}`);
});

//-------------------------------------------------------------------------------------------------

app.ws("/api/stream/:data", (ws, req) => {
  console.log(getTimeStr(), req.params.data);
  const data = JSON.parse(req.params.data);
  const { mode, streamNumber, playbackTime, floor } = data;
  let url;
  let additionalFlags = [];
  if (data.mode === "realtime") {
    if (floor === "1F") {
      url = `rtsp://${RTSP_AUTH}@${RTSP_IP}:2554/h264/ch${streamNumber}/sub/av_stream`;
    } else if (floor === "5F") {
      url = `rtsp://${RTSP_AUTH}@${RTSP_IP}:1554/h264/ch${streamNumber}/main/av_stream`;
    }
    additionalFlags = ["-an", "-vf", "scale='1920:1080'"];
  } else if (data.mode === "playback") {
    if (floor === "1F") {
      url = `rtsp://${RTSP_AUTH}@${RTSP_IP}:2554/Streaming/tracks/${streamNumber}01?starttime=${playbackTime}`;
    } else if (floor === "5F") {
      url = `rtsp://${RTSP_AUTH}@${RTSP_IP}:1554/Streaming/tracks/${streamNumber}01?starttime=${playbackTime}`;
    }
    // additionalFlags = ["-an", "-vf", "scale='720:405'"];
    additionalFlags = ["-an", "-vf", "scale='1080:607.5'"];
  }
  proxy({
    url: url,
    additionalFlags: additionalFlags,
    transport: "tcp",
    verbose: true,
  })(ws)
    .then((res) => {
      console.log("res", res);
    })
    .catch((error) => {
      console.log("error", error);
      ws.send(error);
    });
  // ws.end();
});
//-------------------------------------------------------------------------------------------------
app.get("/ip", (req, res) => {
  res.send(MY_IP);
});
app.get("/wsip", (req, res) => {
  res.send(`${MY_IP}:${PORT}`);
});

const SECRET = "factoryfusionmodel";

app.post("/login", (req, res) => {
  console.log(getTimeStr(), "someone login...");
  const { account, password } = req.body;
  const uniqueAccount = LOGIN_NAME;
  const uniquePassword = LOGIN_PASSWORD;
  const uniquePasswordCrypt = bcrypt.hashSync(uniquePassword, 10);
  if (uniqueAccount === account) {
    // if (uniquePassword === password) {
    if (bcrypt.compareSync(password, uniquePasswordCrypt)) {
      console.log("password verify success");
      let payload = {
        account: account,
      };
      const token = jwt.sign(
        { payload, exp: Math.floor(Date.now() / 1000) + 60 * 60 },
        SECRET
      );
      console.log(getTimeStr(), "login success...");
      res.send({ msg: "login success.", token, statusCode: 200 });
    } else {
      console.warn(getTimeStr(), "login error!!");
      res.send({ msg: "password incorrect.", statusCode: 403 });
    }
  } else {
    console.warn(getTimeStr(), "account doesn't exist!!");
    res.send({ msg: "account doesn't exist.", statusCode: 400 });
  }
});

app.get("/check_login", function (req, res) {
  const token = req.header("Authorization");
  console.log(getTimeStr(), "check_login: ", token);
  if (token) {
    try {
      const decoded = jwt.verify(token, SECRET);
      res.send({ msg: "logged", statusCode: 200, userInfo: decoded.payload });
    } catch (tokenErr) {
      console.warn(getTimeStr(), "Login Error: ", tokenErr);
      res.send({ msg: " please login again.", statusCode: 401 });
    }
  } else {
    console.log("please login again");
    res.send({ msg: "please login again.", statusCode: 402 });
  }
});

module.exports = app;
